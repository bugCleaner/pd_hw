from itertools import combinations
class wire () :
    def __init__(self,n1_in,n2_in,w_in) :
        self.n1 = n1_in
        self.n2 = n2_in
        self.w = w_in

wireList = []

wireList.append(wire('a','c',3))
wireList.append(wire('a','d',4))
wireList.append(wire('b','c',5))
wireList.append(wire('b','d',2))
wireList.append(wire('b','e',1))
wireList.append(wire('c','f',4))
wireList.append(wire('d','e',6))
wireList.append(wire('d','f',3))
wireList.append(wire('e','f',7))
# s = 0
# for e in wireList :
#     s += e.w
# print(s)

nodeList = ['a','b','c','d','e','f']

def calCut (wL,nL) :
    cutSum = 0
    for w in wL :
        n = 0
        if w.n1 in nL :
            n += 1
        if w.n2 in nL :
            n += 1
        if n%2 != 0 :
            cutSum += w.w
    return cutSum

# c = calCut(wireList,['a','b','c'])
# print(c)
mincut = 100
for c in list(combinations(nodeList,3)) :
    if calCut(wireList,c) <= mincut :
        mincut = calCut(wireList,c)
        print("combination is {" + str(c) + "}   mincut: " + str(mincut))